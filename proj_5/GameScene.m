//
//  GameScene.m
//  proj_5
//
//  Created by Yash Panchamia on 3/26/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "GameScene.h"
#define Yaxis -3*(self.size.height/2)/4
#define Rleft -3*(self.size.width/2)/4
#define Rright -(self.size.width/2)/4
#define Oleft (self.size.width/2)/4
#define Oright 3*(self.size.width/2)/4
#define YaxisBlocks 3*(self.size.height/2)/4

@interface GameScene() <SKPhysicsContactDelegate>
{
}
@end

@implementation GameScene
{
    SKNode *bgLayer;
    SKNode *gameOverLayer;
    SKNode *gameLayer;
    SKSpriteNode *bg1;
    SKSpriteNode *bg2;
    SKSpriteNode *redCar;
    SKSpriteNode *orangeCar;
    SKSpriteNode *tryAgain;

}
bool redOnLeft;
bool orgOnLeft;
UITouch *touch;
bool gameOver;
int score;
static const uint32_t redCarCategoty = 1<<0;
static const uint32_t orangeCarCategory = 1<<1;
static const uint32_t blueBlockscategory = 1<<2;
static const uint32_t pointCategory = 1<<3;

- (void)didMoveToView:(SKView *)view
{
    [self setSize:CGSizeMake(1080, 1920)];
    for(SKNode *node in self.children)
    {
        [node removeFromParent];
    }
    self.physicsWorld.contactDelegate = self;
    bgLayer = [SKNode node];
    [self addChild:bgLayer];
    
    gameLayer = [SKNode node];
    [self addChild:gameLayer];
    
    gameOverLayer = [SKNode node];
    gameOverLayer.hidden = YES;
    [self addChild:gameOverLayer];
    
    //BACKGROUND
    SKTexture *bg = [SKTexture textureWithImageNamed:@"trackss"];
    bg1 = [[SKSpriteNode alloc] initWithTexture:bg];
    bg2 = [[SKSpriteNode alloc] initWithTexture:bg];
    bg1.position = CGPointMake(0, 0);
    bg1.zPosition = bg2.zPosition = -20;
    //[bg1 setScale:1.05];
    [bg1 setSize:CGSizeMake(self.size.width, bg1.size.height)];
    bg2.position = CGPointMake(0, bg2.size.height/2-1);
    [bg2 setSize:CGSizeMake(self.size.width, bg2.size.height)];
    [bgLayer addChild:bg2];
    [bgLayer addChild:bg1];
    
    //RED CAR
    SKTexture * redC= [SKTexture textureWithImageNamed:@"red_car"];
    redCar =[[SKSpriteNode alloc]initWithTexture:redC];
    redCar.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:redCar.size];
    redCar.physicsBody.dynamic = YES;
    redOnLeft = YES;
    redCar.physicsBody.affectedByGravity = NO;
    [redCar setScale:0.27];
    redCar.physicsBody.categoryBitMask = redCarCategoty;
    redCar.physicsBody.collisionBitMask = blueBlockscategory | pointCategory;
    redCar.physicsBody.contactTestBitMask = blueBlockscategory | pointCategory;
    redCar.position = CGPointMake(Rleft, Yaxis);
    [self addChild:redCar];
    
    //ORANGE CAR
    SKTexture * orgC= [SKTexture textureWithImageNamed:@"orange_car"];
    orangeCar =[[SKSpriteNode alloc]initWithTexture:orgC];
    orangeCar.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:orangeCar.size];
    orangeCar.physicsBody.dynamic = YES;
    orgOnLeft = YES;
    orangeCar.physicsBody.affectedByGravity = NO;
    [orangeCar setScale:0.27];
    orangeCar.physicsBody.categoryBitMask = orangeCarCategory;
    orangeCar.physicsBody.collisionBitMask = blueBlockscategory | pointCategory;
    orangeCar.physicsBody.contactTestBitMask = blueBlockscategory | pointCategory;
    orangeCar.position = CGPointMake(Oleft, Yaxis);
    [self addChild:orangeCar];
    
    
    
    SKAction *redCarBlocks = [SKAction performSelector:@selector(redCarBlocks) onTarget:self];
    SKAction *delay = [SKAction waitForDuration:arc4random()%5 + 11];
    SKAction *sequence = [SKAction sequence:@[redCarBlocks,delay]];
    SKAction *forever = [SKAction repeatActionForever:sequence];
    [self runAction:forever];
    
    
    SKAction *orangeCarBlocks = [SKAction performSelector:@selector(orangeCarBlocks) onTarget:self];
    SKAction *Orgdelay = [SKAction waitForDuration:arc4random()%5 + 11];
    SKAction *Orgsequence = [SKAction sequence:@[orangeCarBlocks,Orgdelay]];
    SKAction *Orgforever = [SKAction repeatActionForever:Orgsequence];
    [self runAction:Orgforever];
    
    //gameover
    SKSpriteNode *gameOverNode = [[SKSpriteNode alloc] initWithImageNamed:@"game_over"];
    gameOverNode.position = CGPointMake(0,self.size.height/4);
    [gameOverNode setScale:1.2];
    tryAgain = [[SKSpriteNode alloc]initWithImageNamed:@"try_again"];
    tryAgain.position = CGPointMake(0, 0);
    [tryAgain setScale:1.1];
    gameOverLayer.zPosition = 500;
    [gameOverLayer addChild:tryAgain];
    [gameOverLayer addChild:gameOverNode];
    gameOver = NO;
    
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    SKPhysicsBody *block;
    SKPhysicsBody *car;
    
    //BLOCKS
    if((contact.bodyA.categoryBitMask & blueBlockscategory) == blueBlockscategory || (contact.bodyB.categoryBitMask & blueBlockscategory) == blueBlockscategory)
    {
        gameOver = YES;
        gameOverLayer.hidden = NO;
    }
    
    //POINTS
    if((contact.bodyA.categoryBitMask & pointCategory) == pointCategory || (contact.bodyB.categoryBitMask & pointCategory) == pointCategory)
    {
        NSLog(@"contact points");
        if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
        {
            block = contact.bodyB;
            car = contact.bodyA;
        }
        else
        {
            block = contact.bodyA;
            car = contact.bodyB;
        }
        score++;
        SKSpriteNode *skblock = (SKSpriteNode *)block.node;
        [skblock removeFromParent];
        NSLog(@"score = %i",score);
        
    }
}

-(void)orangeCarBlocks
{
    int select = arc4random()%2;
    //NSLog(@"choice=%i",select);
    if(select == 0)
    {
        //NSLog(@"here");
        int choice = arc4random()%2;
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:@"circle"];
        [sprite setScale:0.5];
        sprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:sprite.size];
        sprite.physicsBody.dynamic =YES;
        sprite.physicsBody.affectedByGravity =NO;
        sprite.physicsBody.categoryBitMask = pointCategory;
        sprite.physicsBody.contactTestBitMask =  orangeCarCategory;
        sprite.physicsBody.collisionBitMask = orangeCarCategory;
        sprite.name = @"circlePoints";
        if(choice == 0)
            sprite.position = CGPointMake(Oleft, YaxisBlocks);
        else
            sprite.position = CGPointMake(Oright, YaxisBlocks);
        
        [self addChild:sprite];
    }
    else
    {
        int choice = arc4random()%2;
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:@"square"];
        [sprite setScale:0.5];
        sprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:sprite.size];
        sprite.physicsBody.dynamic =YES;
        sprite.physicsBody.affectedByGravity =NO;
        sprite.physicsBody.categoryBitMask = blueBlockscategory;
        sprite.physicsBody.contactTestBitMask = orangeCarCategory;
        sprite.physicsBody.collisionBitMask = orangeCarCategory;
        sprite.name = @"blueBlock";
        if(choice == 0)
            sprite.position = CGPointMake(Oleft, YaxisBlocks);
        else
            sprite.position = CGPointMake(Oright, YaxisBlocks);
        
        [self addChild:sprite];
    }
}


-(void)redCarBlocks
{
    int select = arc4random()%2;
    //NSLog(@"choice=%i",select);
    if(select == 0)
    {
        //NSLog(@"here");
        int choice = arc4random()%2;
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:@"circle"];
        [sprite setScale:0.5];
        sprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:sprite.size];
        sprite.physicsBody.dynamic =YES;
        sprite.physicsBody.affectedByGravity =NO;
        sprite.physicsBody.categoryBitMask = pointCategory;
        sprite.physicsBody.contactTestBitMask = redCarCategoty ;
        sprite.physicsBody.collisionBitMask = redCarCategoty;
        sprite.name = @"circlePoints";
        if(choice == 0)
            sprite.position = CGPointMake(Rleft, YaxisBlocks);
        else
            sprite.position = CGPointMake(Rright, YaxisBlocks);
        
        [self addChild:sprite];
    }
    else
    {
        int choice = arc4random()%2;
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:@"square"];
        [sprite setScale:0.5];
        sprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:sprite.size];
        sprite.physicsBody.dynamic =YES;
        sprite.physicsBody.affectedByGravity =NO;
        sprite.physicsBody.categoryBitMask = blueBlockscategory;
        sprite.physicsBody.contactTestBitMask = redCarCategoty;
        sprite.physicsBody.collisionBitMask  = redCarCategoty;
        sprite.name = @"blueBlock";
        if(choice == 0)
            sprite.position = CGPointMake(Rleft, YaxisBlocks);
        else
            sprite.position = CGPointMake(Rright, YaxisBlocks);
        
        [self addChild:sprite];
    }
}


-(void)removeBlocks
{
    if(gameOver == NO)
    {
    [self enumerateChildNodesWithName:@"blueBlock" usingBlock:^(SKNode *node, BOOL *stop)
     {
         SKSpriteNode *sprite = (SKSpriteNode*) node;
         if(sprite.position.y < -self.size.height/2)
         {
             [sprite removeFromParent];
         }
         
         else
         {
             sprite.position = CGPointMake(sprite.position.x, sprite.position.y-4);
         }
     
     }];
    
    [self enumerateChildNodesWithName:@"circlePoints" usingBlock:^(SKNode *node, BOOL *stop)
     {
         SKSpriteNode *sprite = (SKSpriteNode*) node;
         if(sprite.position.y < -self.size.height/2)
         {
             [sprite removeFromParent];
         }
         
         else
         {
             sprite.position = CGPointMake(sprite.position.x, sprite.position.y-4);
         }
         
     }];
    }
}

- (void)touchDownAtPoint:(CGPoint)pos {
    
}

- (void)touchMovedToPoint:(CGPoint)pos {
    
}

- (void)touchUpAtPoint:(CGPoint)pos {
   
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self];
    if(gameOver == NO)
    {
    if(touchLocation.x < 0)
    {
        if(redOnLeft == YES)
        {
            redCar.position = CGPointMake(Rright, Yaxis);
            redOnLeft = NO;
        }
        else
        {
            redCar.position = CGPointMake(Rleft, Yaxis);
            redOnLeft = YES;
        }
    }

    else if(touchLocation.x > 0)
    {
        if(orgOnLeft == YES)
        {
            orangeCar.position = CGPointMake(Oleft, Yaxis);
            orgOnLeft = NO;
        }
        else
        {
            orangeCar.position = CGPointMake(Oright, Yaxis);
            orgOnLeft = YES;
        }
    }
    }
    
    else if(gameOver == YES)
    {
        if(CGRectContainsPoint(tryAgain.frame, touchLocation))
        {
            for(SKNode* node in self.children)
            {
                [node removeFromParent];
            }
            
            for(SKSpriteNode* node in self.children)
            {
                [node removeFromParent];
            }
            [self.view.scene removeAllActions];
            SKScene *myscene = [[GameScene alloc] initWithSize:self.view.frame.size];
            [myscene setSize:CGSizeMake(1080, 1920)];
            SKTransition *reveal =[SKTransition crossFadeWithDuration:0.5];
            //reveal.pausesIncomingScene=NO;
            [self.view presentScene:myscene transition:reveal];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
}


-(void)update:(CFTimeInterval)currentTime
{
    if(gameOver == NO)
    {
    //move background tracks
    bg1.position = CGPointMake(0, bg1.position.y -4);
    bg2.position = CGPointMake(0, bg2.position.y -4);
    if(bg1.position.y < -(bg1.size.height/2+self.size.height/2))
    {
        bg1.position = CGPointMake(0, bg2.position.y + bg2.size.height);
    }
    if(bg2.position.y < -(bg2.size.height/2+self.size.height/2))
    {
        bg2.position = CGPointMake(0, bg1.position.y + bg1.size.height);
    }
    
    [self removeBlocks];
    }
}

@end
