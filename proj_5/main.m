//
//  main.m
//  proj_5
//
//  Created by Yash Panchamia on 3/26/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
